﻿namespace FirstMVCAppMy.DAL
{
    public interface IUnitOfWorkFactory
    {
        UnitOfWork Create();
    }
}