﻿using FirstMVCAppMy.DAL.Entities;
using FirstMVCAppMy.DAL.EntitiesConfiguration.Contracts;

namespace FirstMVCAppMy.DAL.EntitiesConfiguration
{
    public class EntityConfigurationsContainer : IEntityConfigurationsContainer
    {
        public IEntityConfiguration<Product> ProductConfiguration { get; }
        public IEntityConfiguration<Category> CategoryConfiguration { get; }
        public IEntityConfiguration<Brand> BrandConfiguration { get; }
        public IEntityConfiguration<Order> OrderConfiguration { get; }
        public IEntityConfiguration<OrderBucket> OrderBucketConfiguration { get; }

        public EntityConfigurationsContainer()
        {
            ProductConfiguration = new ProductConfiguration();
            CategoryConfiguration = new CategoryConfiguration();
            BrandConfiguration = new BrandConfiguration();
            OrderConfiguration = new OrderConfiguration();
            OrderBucketConfiguration = new OrderBucketConfiguration();
        }
    }
}
