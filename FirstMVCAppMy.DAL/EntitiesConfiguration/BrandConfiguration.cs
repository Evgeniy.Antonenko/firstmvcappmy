﻿using FirstMVCAppMy.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FirstMVCAppMy.DAL.EntitiesConfiguration
{
    public class BrandConfiguration : BaseEntityConfiguration<Brand>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Brand> builder)
        {
            builder
                .Property(b => b.Name)
                .HasMaxLength(200)
                .IsRequired();

            builder
                .HasIndex(c => c.Name)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Brand> builder)
        {
            builder
                .HasMany(b => b.Products)
                .WithOne(b => b.Brand)
                .HasForeignKey(b => b.BrandId);
        }
    }
}
