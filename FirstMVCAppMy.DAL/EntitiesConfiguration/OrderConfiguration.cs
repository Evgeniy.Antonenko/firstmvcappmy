﻿using FirstMVCAppMy.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FirstMVCAppMy.DAL.EntitiesConfiguration
{
    public class OrderConfiguration : BaseEntityConfiguration<Order>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Order> builder)
        {
           builder
                .Property(o => o.Description)
                .HasMaxLength(800);
            builder.Property(o => o.Quantity)
                .IsRequired(); 
            builder.Property(o =>o.ProductAmount)
                .HasDefaultValue(0.0M)
                .IsRequired();
            builder.Property(o => o.CreatedOn)
                .IsRequired();
            builder.HasIndex(o => o.ProductAmount).IsUnique(false);

        }
       
        protected override void ConfigureForeignKeys(EntityTypeBuilder<Order> builder)
        {
            builder
                .HasOne(o => o.Product)
                .WithMany(o => o.Orders)
                .HasForeignKey(o => o.ProductId)
                .IsRequired();
        }
    }
}
