﻿using FirstMVCAppMy.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FirstMVCAppMy.DAL.EntitiesConfiguration
{
    public class OrderBucketConfiguration : BaseEntityConfiguration<OrderBucket>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<OrderBucket> builder)
        {
            builder.Property(o => o.ProductBucket)
                .HasMaxLength(2000);
            builder.Property(o => o.TotalAmount)
                .HasDefaultValue(0.0M)
                .IsRequired();
            builder.HasIndex(o => o.Id).IsUnique(false);
        }

    }
}
