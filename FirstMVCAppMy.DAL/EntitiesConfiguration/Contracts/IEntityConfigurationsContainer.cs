﻿using FirstMVCAppMy.DAL.Entities;

namespace FirstMVCAppMy.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<Product> ProductConfiguration { get; }
        IEntityConfiguration<Category> CategoryConfiguration { get; }
        IEntityConfiguration<Brand> BrandConfiguration { get; }
        IEntityConfiguration<Order> OrderConfiguration { get; }
        IEntityConfiguration<OrderBucket> OrderBucketConfiguration { get; }
    }
}
