﻿using FirstMVCAppMy.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FirstMVCAppMy.DAL.EntitiesConfiguration
{
    public class CategoryConfiguration : BaseEntityConfiguration<Category>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Category> builder)
        {
            builder
                .Property(c => c.Name)
                .HasMaxLength(150)
                .IsRequired();

            builder
                .Property(c => c.Description)
                .HasMaxLength(500)
                .IsRequired();

            builder
                .HasIndex(c => c.Name)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Category> builder)
        {
            builder
                .HasMany(c => c.Products)
                .WithOne(c => c.Category)
                .HasForeignKey(c => c.CategoryId)
                .IsRequired();
        }
    }
}
