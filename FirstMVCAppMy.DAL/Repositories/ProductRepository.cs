﻿using System.Collections.Generic;
using System.Linq;
using FirstMVCAppMy.DAL.Entities;
using FirstMVCAppMy.DAL.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;

namespace FirstMVCAppMy.DAL.Repositories
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Products;
        }

        public IEnumerable<Product> GetFullProducts()
        {
            return entities.Include(p => p.Category).Include(p =>p.Brand);
        }

        public Product GetTheMostExpensiveProduct()
        {
            return entities.OrderByDescending(e => e.Price).First();
        }

        public Product GetByIdFullProduct(int id)
        {
            return GetFullProducts().FirstOrDefault(p => p.Id == id);
        }
    }
}
