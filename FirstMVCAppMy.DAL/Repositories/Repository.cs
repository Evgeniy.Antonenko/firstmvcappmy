﻿using System.Collections.Generic;
using System.Linq;
using FirstMVCAppMy.DAL.Entities;
using FirstMVCAppMy.DAL.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;

namespace FirstMVCAppMy.DAL.Repositories
{
    public class Repository<T> : IRepository<T> where T : Entity
    {
        private ApplicationDbContext _context; 
        protected DbSet<T> entities;

        public Repository(ApplicationDbContext context) 
        { 
            _context = context;
        }
        
        public T Create(T entity) 
        { 
            var entityEntry = entities.Add(entity);
            _context.SaveChanges(); 
            return entityEntry.Entity;
        }

        public T GetById(int id)
        {
            return entities.FirstOrDefault(e => e.Id == id);
        }
        
        public IEnumerable<T> GetAll()
        { 
            return entities.ToList();
        }

        public T Update(T entity)
        {
            var entityEntry = _context.Update(entity);
            _context.SaveChanges();
            return entityEntry.Entity;
        }

        public void Remove(T entity)
        {
            entities.Remove(entity);
            _context.SaveChanges();
        }
    }
}
