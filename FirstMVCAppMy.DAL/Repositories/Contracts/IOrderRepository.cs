﻿using System.Collections.Generic;
using FirstMVCAppMy.DAL.Entities;

namespace FirstMVCAppMy.DAL.Repositories.Contracts
{
    public interface IOrderRepository : IRepository<Order>
    {
        IEnumerable<Order> GetAllWithProductAndCustomer();
    }
}
