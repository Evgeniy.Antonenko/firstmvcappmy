﻿using System.Collections.Generic;
using FirstMVCAppMy.DAL.Entities;

namespace FirstMVCAppMy.DAL.Repositories.Contracts
{
    public interface IRepository<T> where T : Entity
    {
        T Create(T entity);

        T GetById(int id);

        IEnumerable<T> GetAll();

        T Update(T entity);

        void Remove(T entity);
    } 
}
