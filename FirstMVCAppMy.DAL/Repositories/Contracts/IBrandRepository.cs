﻿using FirstMVCAppMy.DAL.Entities;

namespace FirstMVCAppMy.DAL.Repositories.Contracts
{
    public interface IBrandRepository : IRepository<Brand>
    {
    }
}
