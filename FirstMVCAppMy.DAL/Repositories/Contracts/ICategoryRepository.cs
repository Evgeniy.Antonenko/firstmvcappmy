﻿using FirstMVCAppMy.DAL.Entities;

namespace FirstMVCAppMy.DAL.Repositories.Contracts
{
    public interface ICategoryRepository : IRepository<Category>
    {
    }
}
