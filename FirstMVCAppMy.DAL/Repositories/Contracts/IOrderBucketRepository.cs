﻿using System.Collections.Generic;
using FirstMVCAppMy.DAL.Entities;

namespace FirstMVCAppMy.DAL.Repositories.Contracts
{
    public interface IOrderBucketRepository : IRepository<OrderBucket>
    {
        IEnumerable<OrderBucket> GetOrderBucketByUserId(int id);
    }
}
