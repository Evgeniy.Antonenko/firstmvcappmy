﻿using System.Collections.Generic;
using FirstMVCAppMy.DAL.Entities;

namespace FirstMVCAppMy.DAL.Repositories.Contracts
{
    public interface IProductRepository : IRepository<Product>
    {
        Product GetTheMostExpensiveProduct();
        IEnumerable<Product> GetFullProducts();
        Product GetByIdFullProduct(int id);
    }
}
