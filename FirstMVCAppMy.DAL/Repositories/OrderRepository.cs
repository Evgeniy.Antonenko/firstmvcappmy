﻿using System.Collections.Generic;
using FirstMVCAppMy.DAL.Entities;
using FirstMVCAppMy.DAL.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;

namespace FirstMVCAppMy.DAL.Repositories
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Orders;
        }

        public IEnumerable<Order> GetAllWithProductAndCustomer()
        {
            return entities
                .Include(o => o.Product)
                .ThenInclude(o => o.Brand);
        }
    }
}
