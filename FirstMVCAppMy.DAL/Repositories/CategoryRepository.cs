﻿using FirstMVCAppMy.DAL.Entities;
using FirstMVCAppMy.DAL.Repositories.Contracts;

namespace FirstMVCAppMy.DAL.Repositories
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Categories;
        }
    }
}
