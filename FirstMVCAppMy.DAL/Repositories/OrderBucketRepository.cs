﻿using System.Collections.Generic;
using System.Linq;
using FirstMVCAppMy.DAL.Entities;
using FirstMVCAppMy.DAL.Repositories.Contracts;

namespace FirstMVCAppMy.DAL.Repositories
{
    public class OrderBucketRepository : Repository<OrderBucket>, IOrderBucketRepository
    {
        public OrderBucketRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.OrderBuckets;
        }

        public IEnumerable<OrderBucket> GetOrderBucketByUserId(int id)
        {
            return entities.Where(e => e.UserId == id);
        }
    }
}
