﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FirstMVCAppMy.DAL.Migrations
{
    public partial class AddBucketFieldToUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Bucket",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Bucket",
                table: "AspNetUsers");
        }
    }
}
