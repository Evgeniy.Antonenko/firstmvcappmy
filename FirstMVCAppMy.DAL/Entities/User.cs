﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace FirstMVCAppMy.DAL.Entities
{
    public class User : IdentityUser<int>
    {
        public string Bucket { get; set; }
    }
}
