﻿using System.Collections.Generic;

namespace FirstMVCAppMy.DAL.Entities
{
    public class Category : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
