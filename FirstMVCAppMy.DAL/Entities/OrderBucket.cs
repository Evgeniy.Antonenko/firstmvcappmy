﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FirstMVCAppMy.DAL.Entities
{
    public class OrderBucket : Entity
    {
        public int UserId { get; set; }
        public string ProductBucket { get; set; }
        public decimal TotalAmount { get; set; }
        public DateTime CreatedOrder { get; set; }
    }
}
