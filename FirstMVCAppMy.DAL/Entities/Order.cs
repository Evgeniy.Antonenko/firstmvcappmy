﻿using System;

namespace FirstMVCAppMy.DAL.Entities
{
    public class Order : Entity
    {
        
        public string? Description { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int Quantity { get; set; }
        public decimal ProductAmount { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
