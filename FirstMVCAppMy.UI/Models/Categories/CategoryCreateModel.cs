﻿using System.ComponentModel.DataAnnotations;

namespace FirstMVCAppMy.UI.Models.Categories
{
    public class CategoryCreateModel
    {
        [Display(Name = "Наименование")]
        [Required(ErrorMessage = "Поле \"Наименование\" должно быть заполненно")]
        public string Name { get; set; }

        [Display(Name = "Описание")]
        [Required(ErrorMessage = "Поле \"Описание\" должно быть заполненно")]
        public string Description { get; set; }
    }
}
