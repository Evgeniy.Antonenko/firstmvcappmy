﻿using System.ComponentModel.DataAnnotations;

namespace FirstMVCAppMy.UI.Models.Categories
{
    public class CategoryModel
    {
        public int Id { get; set; }

        [Display(Name = "Наименование категории")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Описание категории")]
        [Required]
        public string Description { get; set; }
    }
}
