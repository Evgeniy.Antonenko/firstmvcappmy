﻿
using System.ComponentModel.DataAnnotations;

namespace FirstMVCAppMy.UI.Models.Brands
{
    public class BrandModel
    {
        public int Id { get; set; }

        [Display(Name = "Наименование брэнда")]
        [Required]
        public string Name { get; set; }
    }
}
