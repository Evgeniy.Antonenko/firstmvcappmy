﻿using System.ComponentModel.DataAnnotations;

namespace FirstMVCAppMy.UI.Models.Brands
{
    public class BrandCreateModel
    {
        [Display(Name = "Наименование")]
        [Required(ErrorMessage = "Поле \"Наименование\" должно быть заполненно")]
        public string Name { get; set; }
    }
}
