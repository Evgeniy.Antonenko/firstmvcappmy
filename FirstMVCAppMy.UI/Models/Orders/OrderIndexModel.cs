﻿using System;

namespace FirstMVCAppMy.UI.Models.Orders
{
    public class OrderIndexModel
    {
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public DateTime OrderCreatedOn { get; set; }
        public int OrderQuantity { get; set; }
        public string OrderProductAmount { get; set; }
    }
}
