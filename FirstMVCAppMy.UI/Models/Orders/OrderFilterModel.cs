﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FirstMVCAppMy.UI.Models.Orders
{
    public class OrderFilterModel
    {
        [Display(Name = "Сумма товара от")]
        public decimal? ProductAmountFrom { get; set; }

        [Display(Name = "Сумма товара до")]
        public decimal? ProductAmountTo { get; set; }
        public List<OrderIndexModel> Orders { get; set; }
    }
}
