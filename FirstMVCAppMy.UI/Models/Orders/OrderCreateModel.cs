﻿using System.ComponentModel.DataAnnotations;
using FirstMVCAppMy.UI.Models.Products;

namespace FirstMVCAppMy.UI.Models.Orders
{
    public class OrderCreateModel
    {
        public ProductModel Product { get; set; }

        [Required]
        public int ProductId { get; set; }

        [Required(ErrorMessage = "Необходимо указать количество товара")]
        [Display(Name = "Количество товара")]
        public int Quantity { get; set; }

        [Display(Name = "Комментарии покупателя")]
        public string Description { get; set; }
    }
}
