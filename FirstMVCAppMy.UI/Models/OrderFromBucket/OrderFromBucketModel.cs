﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FirstMVCAppMy.UI.Models.OrderFromBucket
{
    public class OrderFromBucketModel
    {
        public int Id { get; set; }

        [Display(Name = "Пользователь")]
        public string UserName { get; set; }

        public string ProductBucket { get; set; }

        [Display(Name = "Дата заказа")]
        public DateTime CreatedOrder { get; set; }

        [Display(Name = "Общая сумма всех заказов")]
        public decimal TotalAmount { get; set; }
    }
}
