﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCAppMy.UI.Models.OrderFromBucket
{
    public class OrderFromBucketDetailsModel
    {
        public int ProductId { get; set; }
        
        [Display(Name = "Название продукта")]
        public string Name { get; set; }

        [Display(Name = "Описание продукта")]
        public string Description { get; set; }

        [Display(Name = "Категория")]
        public string CategoryName { get; set; }

        [Display(Name = "Брэнд")]
        public string BrandName { get; set; }

        [Display(Name = "Название продукта")]
        public decimal Price { get; set; }

        [Display(Name = "Количество")]
        public int Qty { get; set; }

        [Display(Name = "Сумма")]
        public  decimal TotalAmountProduct { get; set; }
    }
}
