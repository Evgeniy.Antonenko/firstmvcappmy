﻿using System.ComponentModel.DataAnnotations;

namespace FirstMVCAppMy.UI.Models.Products
{
    public class ProductModel
    {
        public static string NoBrand = "No Brand";
        public int Id { get; set; }

        [Display(Name = "Наименование")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Описание")]
        [Required]
        public string Description { get; set; }

        [Display(Name = "Категория")]
        [Required]
        public string CategoryName { get; set; }

        [Display(Name = "Брэнд")]
        [Required]
        public string BrandName { get; set; }

        [Display(Name = "Цена")]
        [Required]
        public decimal Price { get; set; }

        [Display(Name = "Количество")]
        [Required]
        public int Qty { get; set; }

        [Display(Name = "Общая сумма продукта")]
        [Required]
        public decimal TotalAmountProduct { get; set; }
    }
}
