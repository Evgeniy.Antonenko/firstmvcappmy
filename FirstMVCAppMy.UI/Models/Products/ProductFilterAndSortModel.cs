﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FirstMVCAppMy.UI.Models.Products
{
    public class ProductFilterAndSortModel
    {
        [Display(Name = "Наименование")]
        public string Name { get; set; }
        [Display(Name = "Категория")]
        public int? CategoryId { get; set; }
        [Display(Name = "Брэнд")]
        public int? BrandId { get; set; }
        [Display(Name = "Цена от")]
        public decimal? PriceFrom { get; set; }
        [Display(Name = "Цена до")]
        public decimal? PriceTo { get; set; }
        public List<ProductModel> Products { get; set; }
        public SelectList CategoriesSelect { get; set; }
        public SelectList BrandsSelect { get; set; }
        public ProductSortState NameSort { get; set; }
        public ProductSortState CategorySort { get; set; }
        public ProductSortState BrandSort { get; set; }
        public ProductSortState PriceSort { get; set; }
        public ProductSortState SortState { get; set; }
        public int? Page { get; set; }
        public ProductPageModel ProductPageModel { get; set; }
    }
}
