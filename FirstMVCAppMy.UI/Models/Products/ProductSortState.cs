﻿
namespace FirstMVCAppMy.UI.Models.Products
{
    public enum ProductSortState
    {
        NameAsc,
        NameDesc,
        CategoryAsc,
        CategoryDesc,
        BrandAsc,
        BrandDesc,
        PriceAsc,
        PriceDesc
    }
}
