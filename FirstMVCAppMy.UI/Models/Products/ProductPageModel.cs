﻿using System;

namespace FirstMVCAppMy.UI.Models.Products
{
    public class ProductPageModel
    {
        public int PageNumber { get; set; }
        public int TotalPages { get; set; }

        public ProductPageModel(int count, int pageNumber, int pageSize)
        {
            PageNumber = pageNumber;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
        }

        public bool HasPreviousPage => PageNumber > 1;

        public bool HasNextPage => PageNumber < TotalPages;
    }
}
