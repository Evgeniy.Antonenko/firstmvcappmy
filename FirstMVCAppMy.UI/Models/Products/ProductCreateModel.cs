﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FirstMVCAppMy.UI.Models.Products
{
    public class ProductCreateModel
    {
        [Display(Name = "Наименование")]
        [Required(ErrorMessage = "Поле \"Наименование\" должно быть заполненно")]
        public string Name { get; set; }

        [Display(Name = "Описание")]
        [Required(ErrorMessage = "Поле \"Описание\" должно быть заполненно")]
        public string Description { get; set; }

        [Display(Name = "Категория")]
        [Required(ErrorMessage = "Поле \"Категория\" должно быть указана")]
        public int CategoryId { get; set; }

        [Display(Name = "Брэнд")]
        public int? BrandId { get; set; }

        [Display(Name = "Цена")]
        [Required(ErrorMessage = "\"Цена\" должна быть указана")]
        [Range(0, Double.PositiveInfinity, ErrorMessage = "Цена не может быть ноль или отрицательной")]
        public decimal Price { get; set; } 
        public SelectList CategoriesSelect { get; set; }
        public SelectList BrandsSelect { get; set; }
    }
}
