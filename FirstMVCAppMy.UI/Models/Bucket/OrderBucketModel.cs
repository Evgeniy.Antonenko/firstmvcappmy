﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FirstMVCAppMy.UI.Models.Products;

namespace FirstMVCAppMy.UI.Models.Bucket
{
    public class OrderBucketModel
    {
        [Display(Name = "Пользователь")]
        
        public string UserName { get; set; }
        public List<ProductModel> ProductModels { get; set; }

        [Display(Name = "Общая сумма выбранных товаров")]
        public decimal TotalAmount { get; set; }
    }
}
