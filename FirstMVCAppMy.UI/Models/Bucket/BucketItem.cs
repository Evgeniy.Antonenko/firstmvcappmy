﻿
namespace FirstMVCAppMy.UI.Models.Bucket
{
    public class BucketItem
    {
        public int ProductId { get; set; }
        public int Qty { get; set; }
    }
}
