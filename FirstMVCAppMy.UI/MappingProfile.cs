﻿using AutoMapper;
using FirstMVCAppMy.DAL.Entities;
using FirstMVCAppMy.UI.Models.Brands;
using FirstMVCAppMy.UI.Models.Categories;
using FirstMVCAppMy.UI.Models.OrderFromBucket;
using FirstMVCAppMy.UI.Models.Orders;
using FirstMVCAppMy.UI.Models.Products;

namespace FirstMVCAppMy
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateProductToProductModelMap();
            CreateProductCreateModelToProduct();
            CreateOrderCreateModelToOrder();
            CreateOrderToOrderIndexModelMap();
            CreateModelMap<Category, CategoryModel>();
            CreateModelMap<Brand, BrandModel>();
            CreateModelMap<Product, ProductEditModel>();
            CreateModelMap<Category, CategoryEditModel>();
            CreateModelMap<OrderBucket, OrderFromBucketModel>();
            CreateModelMap<ProductModel, OrderFromBucketDetailsModel>();
        }

        

        private void CreateProductToProductModelMap()
        {
            CreateMap<Product, ProductModel>()
                .ForMember(target => target.CategoryName, 
                    src => src.MapFrom(p => p.Category.Name))
                .ForMember(target => target.BrandName, 
                    src => src.MapFrom(p => p.BrandId == null ? ProductModel.NoBrand : p.Brand.Name));
        }

        private void CreateModelMap<From, To>()
        {
            CreateMap<From, To>();
            CreateMap<To, From>();
        }
        
        private void CreateProductCreateModelToProduct()
        {
            CreateMap<ProductCreateModel, Product>();
        }

        private void CreateOrderCreateModelToOrder()
        {
            CreateMap<OrderCreateModel, Order>();
        }

        private void CreateOrderToOrderIndexModelMap()
        {
            CreateMap<Order, OrderIndexModel>()
                .ForMember(target => target.Price,
                    src => src.MapFrom(p => p.Product.Price))
                .ForMember(target => target.ProductName,
                    src => src.MapFrom(p => $"{p.Product.Name} {(p.Product.Brand != null ? p.Product.Brand.Name : "NO BRAND")}"))
                .ForMember(target => target.OrderCreatedOn,
                    src => src.MapFrom(p => p.CreatedOn))
                .ForMember(target => target.OrderProductAmount,
                    src => src.MapFrom(p => p.ProductAmount))
                .ForMember(target => target.OrderQuantity, 
                    src => src.MapFrom(p => p.Quantity));
        }
    }
}
