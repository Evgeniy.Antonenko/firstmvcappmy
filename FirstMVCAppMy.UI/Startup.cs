using AutoMapper;
using FirstMVCAppMy.DAL;
using FirstMVCAppMy.DAL.Entities;
using FirstMVCAppMy.DAL.EntitiesConfiguration;
using FirstMVCAppMy.DAL.EntitiesConfiguration.Contracts;
using FirstMVCAppMy.UI.Services.Brands;
using FirstMVCAppMy.UI.Services.Brands.Contracts;
using FirstMVCAppMy.UI.Services.Bucket;
using FirstMVCAppMy.UI.Services.Bucket.Contracts;
using FirstMVCAppMy.UI.Services.Categories;
using FirstMVCAppMy.UI.Services.Categories.Contracts;
using FirstMVCAppMy.UI.Services.OrderFromBucket;
using FirstMVCAppMy.UI.Services.OrderFromBucket.Contracts;
using FirstMVCAppMy.UI.Services.Orders;
using FirstMVCAppMy.UI.Services.Orders.Contracts;
using FirstMVCAppMy.UI.Services.Products;
using FirstMVCAppMy.UI.Services.Products.Contracts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace FirstMVCAppMy.UI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string connectionString = Configuration.GetConnectionString("MainConnectionStrings");
            var optionsBuilder = new DbContextOptionsBuilder();
            optionsBuilder.UseSqlServer(connectionString);
            services.AddScoped<IEntityConfigurationsContainer>(sp => new EntityConfigurationsContainer());
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(connectionString);
            });
            services.AddSingleton<IApplicationDbContextFactory>(
                sp => new ApplicationDbContextFactory(optionsBuilder.Options, new EntityConfigurationsContainer()));
            services.AddSingleton<IUnitOfWorkFactory, UnitOfWorkFactory>();

            services.AddSingleton<IProductService, ProductService>();
            services.AddSingleton<ICategoryService, CategoryService>();
            services.AddSingleton<IBrandService, BrandService>();
            services.AddSingleton<IOrderService, OrderService>();
            services.AddScoped<IBucketService, BucketService>();
            services.AddScoped<IOrderFromBucketService, OrderFromBucketService>();

            services.AddDefaultIdentity<User>(options => options.SignIn.RequireConfirmedAccount = false)
                .AddEntityFrameworkStores<ApplicationDbContext>();
            services.AddControllersWithViews();
            services.AddRazorPages();

            Mapper.Initialize(config => config.AddProfile(new MappingProfile()));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }
    }
}
