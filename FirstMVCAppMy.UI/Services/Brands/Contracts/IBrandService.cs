﻿using System.Collections.Generic;
using FirstMVCAppMy.UI.Models.Brands;
using FirstMVCAppMy.UI.Models.Products;

namespace FirstMVCAppMy.UI.Services.Brands.Contracts
{
    public interface IBrandService
    {
        List<BrandModel> GetBrandsList();
        void CreateBrand(BrandCreateModel model);
        List<ProductModel> GetProductsBrand(int id);
    }
}
