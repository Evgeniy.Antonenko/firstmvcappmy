﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FirstMVCAppMy.DAL;
using FirstMVCAppMy.DAL.Entities;
using FirstMVCAppMy.UI.Models.Brands;
using FirstMVCAppMy.UI.Models.Products;
using FirstMVCAppMy.UI.Services.Brands.Contracts;

namespace FirstMVCAppMy.UI.Services.Brands
{
    public class BrandService : IBrandService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public BrandService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public List<BrandModel> GetBrandsList()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var brands = unitOfWork.Brands.GetAll().ToList();
                return Mapper.Map<List<BrandModel>>(brands);
            }
        }

        public void CreateBrand(BrandCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var brand = Mapper.Map<Brand>(model);
                unitOfWork.Brands.Create(brand);
            }
        }

        public List<ProductModel> GetProductsBrand(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                int brand = id;
                var products = unitOfWork.Products.GetFullProducts().Where(p => p.BrandId == brand);
                return Mapper.Map<List<ProductModel>>(products);
            }
        }
    }
}
