﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstMVCAppMy.DAL;
using FirstMVCAppMy.DAL.Entities;
using FirstMVCAppMy.UI.Models.Bucket;
using FirstMVCAppMy.UI.Models.Products;
using FirstMVCAppMy.UI.Services.Bucket.Contracts;
using FirstMVCAppMy.UI.Services.Products.Contracts;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;

namespace FirstMVCAppMy.UI.Services.Bucket
{
    public class BucketService : IBucketService
    {
        public readonly UserManager<User> _userManager;
        public readonly IUnitOfWorkFactory _unitOfWorkFactory;
        public readonly IProductService _productService;

        public BucketService(UserManager<User> userManager,
            IUnitOfWorkFactory unitOfWorkFactory, IProductService productService)
        {
            _userManager = userManager;
            _unitOfWorkFactory = unitOfWorkFactory;
            _productService = productService;
        }

        public OrderBucketModel GetBucketList (User currentUser)
        {
            OrderBucketModel orderBucket = new OrderBucketModel();
            orderBucket.ProductModels = new List<ProductModel>();
            orderBucket.UserName = currentUser.UserName;
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var products = _productService.CetProductWithCategoryAndBrand(); 
                string currentBucket = currentUser.Bucket;
                if (currentBucket == String.Empty)
                {
                    return orderBucket;
                }
                List<BucketItem> currentItems = JsonConvert.DeserializeObject<List<BucketItem>>(currentBucket);
                foreach (var bucket in currentItems)
                {
                    ProductModel product = products.FirstOrDefault(p => p.Id == bucket.ProductId);
                    product.Qty = bucket.Qty;
                    product.TotalAmountProduct = product.Price * product.Qty;
                    orderBucket.ProductModels.Add(product);
                    orderBucket.TotalAmount += product.TotalAmountProduct;

                }
                
                return orderBucket;
            }
        }

        public async Task AddToBucketAsync(int productId, User user)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var product = unitOfWork.Products.GetById(productId);
                if (product == null)
                    throw new ArgumentOutOfRangeException(nameof(productId), $"No product with id {productId}");

                var bucketItems = string.IsNullOrWhiteSpace(user.Bucket) ?
                    CreateBucket(productId) :
                    UpdateBucket(productId, user.Bucket);

                user.Bucket = JsonConvert.SerializeObject(bucketItems);
                await _userManager.UpdateAsync(user);
            }
        }

        private List<BucketItem> UpdateBucket(int productId, string currentBucket)
        {
            try
            {
                var currentItems = JsonConvert.DeserializeObject<List<BucketItem>>(currentBucket);
                var existingItem = currentItems.FirstOrDefault(c => c.ProductId == productId);
                if (existingItem != null)
                {
                    existingItem.Qty++;
                }
                else
                {
                    currentItems.Add(
                        new BucketItem()
                        {
                            ProductId = productId,
                            Qty = 1
                        }
                    );
                }

                return currentItems;
            }
            catch
            {
                return CreateBucket(productId);
            }
        }

        private List<BucketItem> CreateBucket(int productId)
        {
            return new List<BucketItem>()
            {
                new BucketItem()
                {
                    ProductId = productId,
                    Qty = 1
                }
            };
        }

        public void AddOrderBucket(OrderBucketModel model, User user)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var order = new OrderBucket();
                order.UserId = user.Id;
                order.TotalAmount = model.TotalAmount;
                order.CreatedOrder = DateTime.Now;
                order.ProductBucket = JsonConvert.SerializeObject(model.ProductModels);
                unitOfWork.OrderBuckets.Create(order);
            }
        }
    }
}
