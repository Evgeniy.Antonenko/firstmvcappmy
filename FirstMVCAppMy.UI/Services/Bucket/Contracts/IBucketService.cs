﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FirstMVCAppMy.DAL.Entities;
using FirstMVCAppMy.UI.Models.Bucket;
using FirstMVCAppMy.UI.Models.Products;

namespace FirstMVCAppMy.UI.Services.Bucket.Contracts
{
    public interface IBucketService
    {
        Task AddToBucketAsync(int productId, User user);
        OrderBucketModel GetBucketList(User currentUser);
        void AddOrderBucket(OrderBucketModel model, User user);
    }
}
