﻿using System.Collections.Generic;
using FirstMVCAppMy.DAL.Entities;
using FirstMVCAppMy.UI.Models.Orders;

namespace FirstMVCAppMy.UI.Services.Orders.Contracts
{
    public interface IOrderService
    {
        OrderCreateModel GetOrderCreateModel(int productId);
        Order CreateOrder(OrderCreateModel model);
        List<OrderIndexModel> GetOrdersList();
        public List<OrderIndexModel> SearchOrders(OrderFilterModel model);
    }
}
