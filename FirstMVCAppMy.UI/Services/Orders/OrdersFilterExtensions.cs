﻿using System.Collections.Generic;
using System.Linq;
using FirstMVCAppMy.DAL.Entities;

namespace FirstMVCAppMy.UI.Services.Orders
{
    public static class OrdersFilterExtensions
    {
        public static IEnumerable<Order> ByProductAmountFrom(this IEnumerable<Order> orders, decimal? productAmountFrom)
        {
            if (productAmountFrom.HasValue)
                return orders.Where(o => o.ProductAmount >= productAmountFrom.Value);
            return orders;
        }

        public static IEnumerable<Order> ByProductAmountTo(this IEnumerable<Order> orders, decimal? productAmountTo)
        {
            if (productAmountTo.HasValue)
                return orders.Where(o => o.ProductAmount <= productAmountTo.Value);
            return orders;
        }
    }
}
