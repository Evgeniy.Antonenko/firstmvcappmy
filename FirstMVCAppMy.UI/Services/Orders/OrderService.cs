﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FirstMVCAppMy.DAL;
using FirstMVCAppMy.DAL.Entities;
using FirstMVCAppMy.UI.Models.Orders;
using FirstMVCAppMy.UI.Models.Products;
using FirstMVCAppMy.UI.Services.Orders.Contracts;

namespace FirstMVCAppMy.UI.Services.Orders
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        
        public OrderService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public OrderCreateModel GetOrderCreateModel(int productId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var product = unitOfWork.Products.GetByIdFullProduct(productId);
                
                var orderCreateModel = new OrderCreateModel()
                {
                    ProductId = productId,
                    Product = Mapper.Map<ProductModel>(product)
                };

                return orderCreateModel;
            }
        }

        public Order CreateOrder(OrderCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var order = Mapper.Map<Order>(model);
                order.CreatedOn = DateTime.Now;
                var product = unitOfWork.Products.GetById(order.ProductId);
                order.ProductAmount = order.Quantity * product.Price;

                order = unitOfWork.Orders.Create(order);

                return order;
            }
        }

        public List<OrderIndexModel> GetOrdersList()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var orders = unitOfWork.Orders.GetAllWithProductAndCustomer().ToList();

                return Mapper.Map<List<OrderIndexModel>>(orders);
            }
        }

        public List<OrderIndexModel> SearchOrders(OrderFilterModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                IEnumerable<Order> orders = unitOfWork.Orders.GetAllWithProductAndCustomer();

                orders = orders
                    .ByProductAmountFrom(model.ProductAmountFrom)
                    .ByProductAmountTo(model.ProductAmountTo);

                List<OrderIndexModel> orderModels = Mapper.Map<List<OrderIndexModel>>(orders);
                return orderModels;
            }
        }
    }
}
