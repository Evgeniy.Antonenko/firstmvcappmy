﻿using System.Collections.Generic;
using FirstMVCAppMy.UI.Models.Categories;
using FirstMVCAppMy.UI.Models.Products;

namespace FirstMVCAppMy.UI.Services.Categories.Contracts
{
    public interface ICategoryService
    {
        List<CategoryModel> GetCategoriesList();
        void CreateCategory(CategoryCreateModel model);
        List<ProductModel> GetProductsCategory(int id);
        CategoryEditModel GetCategoryById(int id);
        void EditCategory(CategoryEditModel model);
    }
}
