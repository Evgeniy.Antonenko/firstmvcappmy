﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FirstMVCAppMy.DAL;
using FirstMVCAppMy.DAL.Entities;
using FirstMVCAppMy.UI.Models.Categories;
using FirstMVCAppMy.UI.Models.Products;
using FirstMVCAppMy.UI.Services.Categories.Contracts;

namespace FirstMVCAppMy.UI.Services.Categories
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public CategoryService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public List<CategoryModel> GetCategoriesList()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var categories = unitOfWork.Categories.GetAll().ToList();
                return Mapper.Map<List<CategoryModel>>(categories);
            }
        }

        public void CreateCategory(CategoryCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var category = Mapper.Map<Category>(model);
                unitOfWork.Categories.Create(category);
            }
        }

        public List<ProductModel> GetProductsCategory(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                int category = id;
                var products = unitOfWork.Products.GetFullProducts().Where(p => p.CategoryId == category).ToList();
                return Mapper.Map<List<ProductModel>>(products);
            }
        }

        public CategoryEditModel GetCategoryById(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var category = unitOfWork.Categories.GetById(id);
                var model = Mapper.Map<CategoryEditModel>(category);

                return model;
            }
        }

        public void EditCategory(CategoryEditModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var category = Mapper.Map<Category>(model);
                unitOfWork.Categories.Update(category);
            }
        }
    }
}
