﻿using System.Collections.Generic;
using AutoMapper;
using FirstMVCAppMy.DAL;
using FirstMVCAppMy.DAL.Entities;
using FirstMVCAppMy.UI.Models.OrderFromBucket;
using FirstMVCAppMy.UI.Models.Products;
using FirstMVCAppMy.UI.Services.OrderFromBucket.Contracts;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;

namespace FirstMVCAppMy.UI.Services.OrderFromBucket
{
    public class OrderFromBucketService : IOrderFromBucketService
    {
        public readonly UserManager<User> _userManager;
        public readonly IUnitOfWorkFactory _unitOfWorkFactory;


        public OrderFromBucketService(UserManager<User> userManager, IUnitOfWorkFactory unitOfWorkFactory)
        {
            _userManager = userManager;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public List<OrderFromBucketModel> GetOrderFromBucketList(User currentUser)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                int userId = currentUser.Id;
                var orderBuckets = unitOfWork.OrderBuckets.GetOrderBucketByUserId(userId);
                List<OrderFromBucketModel> orderFromBucketModels = Mapper.Map<List<OrderFromBucketModel>>(orderBuckets);
                
                foreach (var item in orderFromBucketModels)
                {
                    item.UserName = currentUser.UserName;
                }
                
                return orderFromBucketModels;
            }
        }

        public List<OrderFromBucketDetailsModel> GetProductOrderFromBucket(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                List<ProductModel> productOrderBucket = new List<ProductModel>();
                var productsOrder = unitOfWork.OrderBuckets.GetById(id);
                productOrderBucket = JsonConvert.DeserializeObject<List<ProductModel>>(productsOrder.ProductBucket);
                var productOrderFromBucket = Mapper.Map<List<OrderFromBucketDetailsModel>>(productOrderBucket);

                return productOrderFromBucket;
            }
        }
    }
}
