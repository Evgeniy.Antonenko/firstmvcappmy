﻿using System.Collections.Generic;
using FirstMVCAppMy.DAL.Entities;
using FirstMVCAppMy.UI.Models.OrderFromBucket;

namespace FirstMVCAppMy.UI.Services.OrderFromBucket.Contracts
{
    public interface IOrderFromBucketService
    {
        List<OrderFromBucketModel> GetOrderFromBucketList(User currentUser);
        List<OrderFromBucketDetailsModel> GetProductOrderFromBucket(int id);
    }
}
