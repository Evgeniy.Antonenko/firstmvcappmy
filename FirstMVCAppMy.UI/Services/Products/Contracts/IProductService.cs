﻿using System.Collections.Generic;
using FirstMVCAppMy.UI.Models.Products;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FirstMVCAppMy.UI.Services.Products.Contracts
{
    public interface IProductService
    {
        public ProductFilterAndSortModel GetProductFilterSortPageModel(ProductFilterAndSortModel model, ProductSortState sortState);
        ProductCreateModel GetProductCreateModel();
        void CreateProduct(ProductCreateModel model);
        SelectList GetCategoriesSelect();
        SelectList GetBrandsSelect();
        ProductEditModel GetProductById(int id);
        void EditProduct(ProductEditModel model);
        List<ProductModel> CetProductWithCategoryAndBrand();
    }
}