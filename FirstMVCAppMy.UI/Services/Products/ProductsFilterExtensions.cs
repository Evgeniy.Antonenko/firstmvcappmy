﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstMVCAppMy.DAL;
using FirstMVCAppMy.DAL.Entities;
using FirstMVCAppMy.UI.Models.Products;

namespace FirstMVCAppMy.UI.Services.Products
{
    public static class ProductsFilterExtensions
    {
        public static IEnumerable<Product> ByPriceFrom(this IEnumerable<Product> products, decimal? priceFrom)
        {
            if (priceFrom.HasValue)
                return products.Where(p => p.Price >= priceFrom.Value);
            return products;
        }

        public static IEnumerable<Product> ByPriceTo(this IEnumerable<Product> products, decimal? priceTo)
        {
            if (priceTo.HasValue)
                return products.Where(p => p.Price <= priceTo.Value);
            return products;
        }

        public static IEnumerable<Product> ByName(this IEnumerable<Product> products, string name)
        {
            if (!string.IsNullOrWhiteSpace(name))
                return products.Where(p => p.Name.Contains(name));
            return products;
        }

        public static IEnumerable<Product> ByCategoryId (this IEnumerable<Product> products, UnitOfWork unitOfWork, int? categoryId)
        {
            if (categoryId.HasValue)
            {
                var category = unitOfWork.Categories.GetById(categoryId.Value);
                if (category== null)
                    throw new ArgumentOutOfRangeException(nameof(categoryId), $"No catagory with Id {categoryId}");
                return products.Where(p => p.CategoryId == categoryId);
            }
            return products;
        }

        public static IEnumerable<Product> ByBrandId(this IEnumerable<Product> products, UnitOfWork unitOfWork, int? brandId)
        {
            if (brandId.HasValue)
            {
                var brand = unitOfWork.Brands.GetById(brandId.Value);
                if (brand == null)
                    throw new ArgumentOutOfRangeException(nameof(brandId), $"No brand with Id {brandId}");
                return products.Where(p => p.BrandId == brandId);
            }
            return products;
        }

        public static ProductFilterAndSortModel Sort (this List<ProductModel> productModels, ProductSortState sortState)
        {
            var productFilterAndSortModel = new ProductFilterAndSortModel()
            {
                NameSort = sortState == ProductSortState.NameAsc ? ProductSortState.NameDesc : ProductSortState.NameAsc,
                CategorySort = sortState == ProductSortState.CategoryAsc ? ProductSortState.CategoryDesc : ProductSortState.CategoryAsc,
                BrandSort = sortState == ProductSortState.BrandAsc ? ProductSortState.BrandDesc : ProductSortState.BrandAsc,
                PriceSort = sortState == ProductSortState.PriceAsc ? ProductSortState.PriceDesc : ProductSortState.PriceAsc
            };
            
            switch (sortState)
            {
                case ProductSortState.NameAsc:
                    productModels = productModels.OrderBy(p => p.Name).ToList();
                    break;
                case ProductSortState.NameDesc:
                    productModels = productModels.OrderByDescending(p => p.Name).ToList();
                    break;
                case ProductSortState.CategoryAsc:
                    productModels = productModels.OrderBy(p => p.CategoryName).ToList();
                    break;
                case ProductSortState.CategoryDesc:
                    productModels = productModels.OrderByDescending(p => p.CategoryName).ToList();
                    break;
                case ProductSortState.BrandAsc:
                    productModels = productModels.OrderBy(p => p.BrandName).ToList();
                    break;
                case ProductSortState.BrandDesc:
                    productModels = productModels.OrderByDescending(p => p.BrandName).ToList();
                    break;
                case ProductSortState.PriceAsc:
                    productModels = productModels.OrderBy(p => p.Price).ToList();
                    break;
                case ProductSortState.PriceDesc:
                    productModels = productModels.OrderByDescending(p => p.Price).ToList();
                    break;
            }

            productFilterAndSortModel.Products = productModels;
            return productFilterAndSortModel;
        }
    }
}