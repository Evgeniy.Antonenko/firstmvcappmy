﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FirstMVCAppMy.DAL;
using FirstMVCAppMy.DAL.Entities;
using FirstMVCAppMy.UI.Models.Products;
using FirstMVCAppMy.UI.Services.Products.Contracts;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FirstMVCAppMy.UI.Services.Products
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public ProductService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public ProductFilterAndSortModel GetProductFilterSortPageModel(ProductFilterAndSortModel model, ProductSortState sortState)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                IEnumerable<Product> products = unitOfWork.Products.GetFullProducts().ToList();

                products = products
                    .ByPriceFrom(model.PriceFrom)
                    .ByPriceTo(model.PriceTo)
                    .ByName(model.Name)
                    .ByCategoryId(unitOfWork, model.CategoryId)
                    .ByBrandId(unitOfWork, model.BrandId);

                List<ProductModel> productModels = Mapper.Map<List<ProductModel>>(products);

                var productFilterAndSortModel = productModels.Sort(sortState);

                int pageSize = 20;
                int count = products.Count();
                int page = model.Page.HasValue ? model.Page.Value : 1;
                ProductPageModel productPageModel = new ProductPageModel(count, page, pageSize);
                productFilterAndSortModel.Products = productFilterAndSortModel.Products.Skip((page - 1) * pageSize).Take(pageSize).ToList();

                productFilterAndSortModel.Name = model.Name;
                productFilterAndSortModel.CategoryId = model.CategoryId;
                productFilterAndSortModel.BrandId = model.BrandId;
                productFilterAndSortModel.PriceFrom = model.PriceFrom;
                productFilterAndSortModel.PriceTo = model.PriceTo;
                productFilterAndSortModel.CategoriesSelect = GetCategoriesSelect();
                productFilterAndSortModel.BrandsSelect = GetBrandsSelect();
                productFilterAndSortModel.SortState = sortState;
                productFilterAndSortModel.ProductPageModel = productPageModel;
                productFilterAndSortModel.Page = page;

                return productFilterAndSortModel;
            }
        }

        public ProductCreateModel GetProductCreateModel()
        {
            return new ProductCreateModel()
            {
                CategoriesSelect = GetCategoriesSelect(),
                BrandsSelect = GetBrandsSelect()
            };
        }

        public void CreateProduct(ProductCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var product = Mapper.Map<Product>(model);
                unitOfWork.Products.Create(product);
            }
        }

        public SelectList GetCategoriesSelect()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var catagories = unitOfWork.Categories.GetAll().ToList();
                return new SelectList(catagories, nameof(Category.Id), nameof(Category.Name));
            }
        }

        public SelectList GetBrandsSelect()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var brands = unitOfWork.Brands.GetAll().ToList();
                return new SelectList(brands, nameof(Brand.Id), nameof(Brand.Name));
            }
        }

        public ProductEditModel GetProductById(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var product = unitOfWork.Products.GetById(id);
                var model = Mapper.Map<ProductEditModel>(product);
                var categories = unitOfWork.Categories.GetAll().ToList();
                var brands = unitOfWork.Brands.GetAll().ToList();

                model.CategoriesSelect = new SelectList(
                    categories, nameof(Category.Id), nameof(Category.Name), model.CategoryId);

                model.BrandsSelect = new SelectList(
                    brands, nameof(Brand.Id), nameof(Brand.Name), model.BrandId);

                return model;
            }
        }

        public void EditProduct(ProductEditModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var product = Mapper.Map<Product>(model);
                unitOfWork.Products.Update(product);
            }
        }

        public List<ProductModel> CetProductWithCategoryAndBrand()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var products = unitOfWork.Products.GetFullProducts();
                return Mapper.Map<List<ProductModel>>(products);
            }
        }

    }
}
