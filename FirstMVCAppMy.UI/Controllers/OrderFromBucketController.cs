﻿using System.Threading.Tasks;
using FirstMVCAppMy.DAL.Entities;
using FirstMVCAppMy.UI.Services.OrderFromBucket.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCAppMy.UI.Controllers
{
    public class OrderFromBucketController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IOrderFromBucketService _orderFromBucketService;

        public OrderFromBucketController(UserManager<User> userManager, IOrderFromBucketService orderFromBucketService)
        {
            _userManager = userManager;
            _orderFromBucketService = orderFromBucketService;
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            User currentUser = await _userManager.GetUserAsync(User);
            var orderFromBucketModels = _orderFromBucketService.GetOrderFromBucketList(currentUser);
            return View(orderFromBucketModels);
        }

        [Authorize]
        [HttpGet]
        public IActionResult Details(int id)
        {
            var orderFromBucketDetails = _orderFromBucketService.GetProductOrderFromBucket(id);
            return View(orderFromBucketDetails);
        }
    }
}
