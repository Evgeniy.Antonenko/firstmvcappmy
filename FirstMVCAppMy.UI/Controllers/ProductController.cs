﻿using System;
using FirstMVCAppMy.UI.Models.Products;
using FirstMVCAppMy.UI.Services.Products.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCAppMy.UI.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductService _productService;
        
        public ProductController(IProductService productService)
        {
            if (productService == null)
                throw new ArgumentNullException(nameof(productService));
            
            _productService = productService;
        }

        //[Route("Search/{name?}/{categoryId?}/{brandId?}/{priceFrom?}/{priceTo?}")]
        public IActionResult Index(ProductFilterAndSortModel model, ProductSortState productSortState = ProductSortState.NameAsc)
        {
            try
            {
                var products = _productService.GetProductFilterSortPageModel(model, productSortState);

                return View(products);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                ViewBag.BadRequestMessage = ex.Message;
                return View("BadRequest");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        public IActionResult CreateProduct()
        {
            var model = _productService.GetProductCreateModel();
            return View(model);
        }
        
        [HttpPost]
        public IActionResult CreateProduct(ProductCreateModel model)
        {
            try
            {
                _productService.CreateProduct(model); 
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        //[Route("Edit/{id?}")]
        public IActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                ViewBag.BadRequestMessage = "Product id cannot be NULL";
                return View("BadRequest");
            }

            var productEditModel = _productService.GetProductById(id.Value);

            return View(productEditModel);
        }

        [HttpPost]
        public IActionResult Edit(ProductEditModel model)
        {
            try
            {
                _productService.EditProduct(model);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}