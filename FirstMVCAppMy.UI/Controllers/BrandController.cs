﻿using System;
using FirstMVCAppMy.UI.Models.Brands;
using FirstMVCAppMy.UI.Services.Brands.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCAppMy.UI.Controllers
{
    public class BrandController : Controller
    {
        private readonly IBrandService _brandService;

        public BrandController(IBrandService brandService)
        {
            if (brandService == null)
                throw new ArgumentNullException(nameof(brandService));
            _brandService = brandService;
        }

        public IActionResult Index()
        {
            var model = _brandService.GetBrandsList();
            return View(model);
        }

        public IActionResult CreateBrand()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateBrand(BrandCreateModel model)
        {
            try
            {
                _brandService.CreateBrand(model);
                
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        public IActionResult ProductsBrand(int id)
        {
            var model = _brandService.GetProductsBrand(id);
            return View(model);
        }
    }
}