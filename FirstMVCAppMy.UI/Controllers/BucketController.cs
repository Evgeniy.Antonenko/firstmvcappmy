﻿using System;
using System.Threading.Tasks;
using FirstMVCAppMy.DAL.Entities;
using FirstMVCAppMy.UI.Models.Bucket;
using FirstMVCAppMy.UI.Services.Bucket.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCAppMy.UI.Controllers
{
    public class BucketController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IBucketService _bucketService;

        public BucketController(UserManager<User> userManager, IBucketService bucketService)
        {
            _userManager = userManager;
            _bucketService = bucketService;
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Add(int? productId)
        {
            if (!productId.HasValue)
            {
                ViewBag.BadRequestMessage = "Product Id can not be NULL";
                return View("BadRequest");
            }

            try
            {
                User currentUser = await _userManager.GetUserAsync(User);
                await _bucketService.AddToBucketAsync(productId.Value, currentUser);
                return RedirectToAction("Index", "Product");
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            User currentUser = await _userManager.GetUserAsync(User);
            var deferredProductModels = _bucketService.GetBucketList(currentUser);
            return View(deferredProductModels);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Index(OrderBucketModel model)
        {
            User currentUser = await _userManager.GetUserAsync(User);
            var bucketItem = _bucketService.GetBucketList(currentUser);
            _bucketService.AddOrderBucket(bucketItem, currentUser);
            currentUser.Bucket = String.Empty;
            await _userManager.UpdateAsync(currentUser);
            return RedirectToAction("Index", "Product");
        }


    }
}
