﻿using System;
using FirstMVCAppMy.UI.Models.Orders;
using FirstMVCAppMy.UI.Services.Orders.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCAppMy.UI.Controllers
{
    public class OrderController : Controller
    {
        private readonly IOrderService _orderService;
        
        public OrderController(IOrderService orderService)
        {
            if (orderService == null)
                throw new ArgumentNullException(nameof(orderService));

            _orderService = orderService;
        }

        //[HttpGet("Orders")]
        public IActionResult Index(OrderFilterModel model)
        {
            try
            {
                var orders = _orderService.SearchOrders(model);
                model.Orders = orders;
                return View(model);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                ViewBag.BadRequestMessage = ex.Message;
                return View("BadRequest");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        //[HttpGet("Order/{id?}")]
        public IActionResult CreateOrder(int? productId)
        {
            if (!productId.HasValue)
                throw new ArgumentOutOfRangeException(nameof(productId));

            var model = _orderService.GetOrderCreateModel(productId.Value);

            return View(model);
        }

        [HttpPost]
        public IActionResult CreateOrder(OrderCreateModel model)
        {
            _orderService.CreateOrder(model);

            return RedirectToAction("Index", "Product");
        }
    }
}
