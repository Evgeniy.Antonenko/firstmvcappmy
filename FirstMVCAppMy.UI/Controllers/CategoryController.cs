﻿using System;
using FirstMVCAppMy.UI.Models.Categories;
using FirstMVCAppMy.UI.Services.Categories.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCAppMy.UI.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;
        
        public CategoryController(ICategoryService categoryService)
        {
            if (categoryService == null)
                throw new ArgumentNullException(nameof(categoryService));
            _categoryService = categoryService;
        }
        
        public IActionResult Index()
        {
            var model = _categoryService.GetCategoriesList();
            return View(model);
        }

        [HttpGet]
        public IActionResult CreateCategory()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateCategory(CategoryCreateModel model)
        {
            try
            {
                _categoryService.CreateCategory(model);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        public IActionResult EditCategory(int? id)
        {
            if (!id.HasValue)
            {
                ViewBag.BadRequestMessage = "Category id cannot be NULL";
                return View("BadRequest");
            }

            var categoryEditModel = _categoryService.GetCategoryById(id.Value);

            return View(categoryEditModel);
        }

        [HttpPost]
        public IActionResult EditCategory(CategoryEditModel model)
        {
            try
            {
                _categoryService.EditCategory(model);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        public IActionResult ProductsCategory(int id)
        {
            var model = _categoryService.GetProductsCategory(id);
            return View(model);
        }
    }
}